from tkinter.messagebox import askyesno
from pages.LoginFrame import LoginFrame
from app.database.db_manager import DatabaseManager
from app.globals.auth import auth
from pages.ClientFrame import ClientFrame
from pages.UserFrame import UserFrame
from pages.ReportFrame import ReportFrame
import customtkinter
from PIL import ImageTk, Image 
import os
import bcrypt
from tkinter import messagebox

class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()
        self.title("Land Management")
        width_of_window = 1000
        height_of_window = 680
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        x_coordinate = (screen_width/2)-(width_of_window/2)
        y_coordinate = (screen_height/2)-(height_of_window/2)
        self.geometry("%dx%d+%d+%d" %(width_of_window,height_of_window,x_coordinate,y_coordinate))
        self.login_rank = None
        self.login_id = None

        # self.geometry("700x700")
        # Set minimum window size
        self.minsize(1000,680)
        self.icon_size = 25

        # set grid layout 1x2
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=2)
        
        #Make connection to database
        self.database_query = DatabaseManager()
        self.logged_in = False
                       
        # load images with light and dark mode image
        image_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "images")
        self.image_path  = image_path
            
        self.logo_image = customtkinter.CTkImage(
            
            Image.open(os.path.join(image_path,
            "CustomTkinter_logo_single.png")),size=(26, 26))
        
        self.home_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "home_dark.png")),
            dark_image=Image.open(os.path.join(image_path, "home_light.png")), 
            size=(self.icon_size, self.icon_size))
                
        self.add_user_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "add-user-dark.png")),
            dark_image=Image.open(os.path.join(image_path, "add-user-light.png")),
            size=(self.icon_size, self.icon_size)
        )

        self.clients_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "clients-icon.png")),
            dark_image=Image.open(os.path.join(image_path, "clients-icon.png")),
            size=(self.icon_size, self.icon_size)
        )

        self.pending_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "pending-icon.png")),
            dark_image=Image.open(os.path.join(image_path, "pending-icon.png")),
            size=(self.icon_size, self.icon_size)
        )

        self.delete_icon = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "delete-icon.png")),
            dark_image=Image.open(os.path.join(image_path, "delete-icon.png")), 
            size=(self.icon_size, self.icon_size))

        self.guest_image = customtkinter.CTkImage(

            light_image=Image.open(os.path.join(image_path, 
            "guest-icon.png")),
            dark_image=Image.open(os.path.join(image_path, "guest-icon.png")),
            size=(self.icon_size, self.icon_size)
        )

        self.gear_icon_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path, 
            "gear-icon-dark.png")),
            dark_image=Image.open(os.path.join(image_path, "gear-icon-light.png")),
            size=(30,30)
        )
                
        self.power_off_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path,"power-off.png")),
            dark_image=Image.open(os.path.join(image_path,"power-off.png")),
            size=(30,30)
        )

        self.report_icon_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path,"reports-icon.png")),
            dark_image=Image.open(os.path.join(image_path,"reports-icon.png")),
            size=(self.icon_size, self.icon_size)
        )

        self.users_icon_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path,"users-icon.png")),
            dark_image=Image.open(os.path.join(image_path,"users-icon.png")),
            size=(30,30)
        )

        self.print_icon_image = customtkinter.CTkImage(
            light_image=Image.open(os.path.join(image_path,"print-icon.png")),
            dark_image=Image.open(os.path.join(image_path,"print-icon.png")),
            size=(30,30)
        )
        
        # create navigation frame
        self.navigation_frame = customtkinter.CTkFrame(
            self, 
            corner_radius=0,
            width=1000
        )
        
        self.navigation_frame.rowconfigure(4, weight=1)

        self.navigation_frame_label = customtkinter.CTkLabel(

            self.navigation_frame, text="Student Attendance Management", 
            # image=self.logo_image,
            compound="left", font=customtkinter.CTkFont(size=15, weight="bold"))
        
        self.navigation_frame_label.grid(row=0, column=0, padx=(10,10), pady=20)

        #Create navigation buttons
        self.home_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40,
            border_spacing=10, text="Home",
            fg_color="transparent", text_color=("gray10", "gray90"), 
            hover_color=("gray70", "gray30"),
            image=self.home_image, anchor="new", command=lambda:self.select_frame_by_name('home')
        )
        
        self.frame_2_button = customtkinter.CTkButton(
            self.navigation_frame, 
            corner_radius=0, height=40, border_spacing=10, text="Students",
            fg_color="transparent", text_color=("gray10", "gray90"), 
            hover_color=("gray70", "gray30"),
            image=self.guest_image, anchor="w",command=lambda:self.select_frame_by_name('frame_2')
        )
        
        self.frame_2_button.grid(row=1, column=0, sticky="new")

        self.frame_3_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Units",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.pending_image, anchor="w", command=lambda:self.select_frame_by_name('frame_3')
        )

        self.frame_4_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Lecturers",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.clients_image, anchor="w", command=lambda:self.select_frame_by_name('frame_4')
        )
        
        self.frame_4_button.grid(row=3, column=0, sticky="new")


        self.frame_5_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Attendance",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.report_icon_image, anchor="w", command=lambda:self.select_frame_by_name('frame_5')
        )
        
        self.frame_5_button.grid(row=4, column=0, sticky="new")
       
        #place bootom of page
        self.frame_6_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Users",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.users_icon_image, anchor="w", command=lambda:self.select_frame_by_name('frame_6')
        )
        

        self.frame_7_button = customtkinter.CTkButton(
            self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Settings",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.gear_icon_image, anchor="w", command=lambda:self.select_frame_by_name('frame_7')
        )
        
        self.frame_7_button.grid(row=6, column=0, sticky="ews")

        #create Power off button
        self.power_off_button = customtkinter.CTkButton(
            master=self.navigation_frame,
            corner_radius=0, height=40, border_spacing=10, text="Exit",
            fg_color="transparent", text_color=("gray10", "gray90"), hover_color=("gray70", "gray30"),
            image=self.power_off_image, anchor="w", command=self.exit_app
        )

        self.power_off_button.grid(row=7,column=0,sticky='ews',pady=(0,20))

        #create apperance mode menu
        self.appearance_mode_menu = customtkinter.CTkOptionMenu(
            self.navigation_frame,
            values=["Light", "Dark", "System"],
            command=self.change_appearance_mode_event)

        self.appearance_mode_menu.grid(row=9, column=0,sticky="s")

        """Frames begin here"""
        # create home frame
        self.home_frame = customtkinter.CTkFrame(
            self, 
            corner_radius=0
        )
              
        self.home_frame.grid_columnconfigure(0, weight=1)

        self.large_test_image = customtkinter.CTkImage(
            Image.open(os.path.join(image_path,
            "login-image.png")),size=(500, 350)) #width height

        #create a login frame
        self.login_frame = LoginFrame(
            master=self.home_frame,
            image=self.large_test_image,
            corner_radius = 0,
            fg_color="gray20",
            login_callback=self.login_callback
        )
        
        self.login_frame.grid(row=0,column=0, padx=0, pady=25)
       
        # select default frame
        self.select_frame_by_name("home")
    
    def login_callback(self, email=None, password=None):
        if password == '' or email == '':
            messagebox.showerror(title="Error", message="All fields are required !",parent=self)
            return
        
        user = self.database_query.user_exists(login_email=email)
        if user == False:
            messagebox.showerror(title="Error", message="Incorrect Username or password!",parent=self)
            return
       
        hashed_password_bytes = user[5]
        """ convert the hashed password to a bytes object if it's a string"""
        if isinstance(hashed_password_bytes, str):
           hashed_password_bytes = bytes(hashed_password_bytes, 'utf-8')

        """verify the input password against the hashed password"""
        if bcrypt.checkpw(password.encode('utf-8'), hashed_password_bytes):
            auth['login_rank'] = user[4]
            auth['login_id'] = user[0]
            self.place_navitems_to_vaframe()
            self.navigation_frame.grid(row=0, column=0, sticky="nsew")
            self.select_frame_by_name("frame_2")    
            return
        else:
            messagebox.showerror(title="Error", message="Incorrect Username or password!",parent=self)
            return
        # self.place_navitems_to_vaframe()
        # self.navigation_frame.grid(row=0, column=0, sticky="nsew")
        # self.select_frame_by_name("frame_2") 
                    
    
    def place_navitems_to_vaframe(self):
        if auth['login_rank'] !=1:
            self.frame_3_button.grid(row=2, column=0, sticky="new")
            self.frame_6_button.grid(row=5, column=0, sticky="ews")


    def select_frame_by_name(self, name):
        # set button color for selected button
        self.home_button.configure(fg_color=("gray75", "gray25") if name == "home" else "transparent")
        self.frame_2_button.configure(fg_color=("gray75", "gray25") if name == "frame_2" else "transparent")
        self.frame_3_button.configure(fg_color=("gray75", "gray25") if name == "frame_3" else "transparent")
        self.frame_4_button.configure(fg_color=("gray75", "gray25") if name == "frame_4" else "transparent")
        self.frame_5_button.configure(fg_color=("gray75", "gray25") if name == "frame_5" else "transparent")
        self.frame_6_button.configure(fg_color=("gray75", "gray25") if name == "frame_6" else "transparent")
        # show selected frame
        if name == "home":
            self.home_frame.grid(row=0, column=1, sticky="nsew")
            return
               
        elif name == "frame_5":
            #create forth frame
            self.forth_frame = ReportFrame(self,database_query=self.database_query)
            self.forth_frame.grid(row=0, column=1, sticky="nsew")
            return
        
        elif name == "frame_6":
            #create forth frame
            self.sixth_frame = UserFrame(self,database_query=self.database_query)
            self.sixth_frame.grid(row=0, column=1, sticky="nsew")
            return
        
        else:
            self.second_frame = ClientFrame(self,page=name,database_query=self.database_query,image_path=self.image_path)
            self.second_frame.grid(row=0, column=1, sticky="nsew")
            return


    def change_appearance_mode_event(self, new_appearance_mode):
        customtkinter.set_appearance_mode(new_appearance_mode)
    

    def exit_app(self):
        answer = askyesno("Confirm exit","Are you sure you want to exit the application !")
        if answer:
            self.quit()
        return

if __name__ == "__main__":
    app = App()
    app.mainloop()