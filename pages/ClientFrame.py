from customtkinter import CTkFrame,CTkEntry,CTkButton
from tkinter import messagebox
from app.widgets.tree_view import TreeView
from pages.PlotFrame import PlotFrame
from pages.Forms import ClientForm,PlotForm,MoveToClientsForm
from tkinter import PhotoImage
from tkinter import END,Menu
import os

class ClientFrame(CTkFrame):

    def __init__(self,*args,page,database_query,image_path,**kwargs):
        super().__init__(*args, **kwargs)
        self.corner_radius = 0 
        self.fg_color = "transparent"
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.page = page
        self.parent_dir = image_path
        delete_image_path = os.path.join(self.parent_dir,"delete-icon.png")
        edit_image_path = os.path.join(self.parent_dir,"edit-icon.png")
        cancel_image_path =  os.path.join(self.parent_dir,"cancel-icon.png")
        add_image_path =  os.path.join(self.parent_dir,"add-icon.png")
        confirm_image_path =  os.path.join(self.parent_dir,"confirm-icon.png")
        pay_image_path =  os.path.join(self.parent_dir,"pay-icon.png")
        transfer_image_path =  os.path.join(self.parent_dir,"transfer-icon.png")

        self.delete_icon = PhotoImage(file=delete_image_path).subsample(3, 3)
        self.edit_icon = PhotoImage(file=edit_image_path).subsample(5, 5)
        self.cancel_icon = PhotoImage(file=cancel_image_path).subsample(5, 5)
        self.add_icon = PhotoImage(file=add_image_path).subsample(5, 5)
        self.pay_icon = PhotoImage(file=pay_image_path).subsample(3, 3)
        self.confirm_icon = PhotoImage(file=confirm_image_path).subsample(2, 2)
        self.transfer_icon = PhotoImage(file=transfer_image_path).subsample(5, 5)

        self.database_query = database_query
        self.plots_section =self.columns=self.client_add_form = self.update_plots_table_callback =  self.add_plot_to_client_form= None            
       
        self.configure_active_frame()       
        # Create search bar
        self.search_bar = CTkEntry(
            self,
            width=250, fg_color="white",text_color="black",
            placeholder_text="Search by ID number or student number")
        
        self.search_bar.grid(row=0, column=0, pady=(22,0),padx=(0,20), sticky="e")

        #create clients table
        self.clients_table = TreeView(self, columns=['#'] + self.columns)
        self.clients_table.column("#", width=20, minwidth=20)
        
        self.clients_table.grid(row=1, column=0, sticky="nsew",pady=(10,20),padx=(0,20))

        self.clients_table.bind('<Double-1>', lambda event: self.show_plots_for_client())

        self.clients_table.bind("<Button-3>", self.clients_table_on_right_click)
    
        self.set_frame_data()
               
    def configure_active_frame(self):
        #create a add client button
        if self.page == 'frame_2':
            self.columns=["First name","Middle Name","Last name", "ID No.","KRA No.","Date added"]
            self.add_client_button = CTkButton(master=self, text="New Student",command=self.open_client_add_form)
            self.add_client_button.grid(row=0, column=0, pady=(25,0), padx=10, sticky="w")
        
        if self.page == 'frame_3':
            self.columns=["First name","Middle Name","Last name", "ID No.","KRA No.","Date added"]        
        
        if self.page == "frame_4":
            self.columns=["First name","Middle Name", "Last name", "ID No.","Student No","Member No.","Date added","Member since"]


    def set_frame_data(self):
        if self.page == 'frame_2':
            data =  self.database_query.fetch_data(
            '''
            SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
            clients.kra_pin,clients.date_added  
            FROM clients WHERE clients.client_status = ? ORDER BY id DESC''',(0,))
            self.hydrate_clients_table(data=data)
        
        if self.page == 'frame_3':
            data =  self.database_query.fetch_data(
             '''
            SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
            clients.kra_pin,clients.date_added  
            FROM clients WHERE clients.client_status = ? ORDER BY id DESC''',(1,))
            self.hydrate_clients_table(data=data)
        
        if self.page == "frame_4":
            data =  self.database_query.fetch_data(
            '''
            SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
            clients.folio_number,clients.member_number,clients.date_added,clients.member_since 
            FROM clients WHERE clients.client_status = ? ORDER BY id DESC
            ''',(2,))
            self.hydrate_clients_table(data=data)
    
    def hydrate_clients_table(self, data=None):
        if data is None:
           data=self.database_query.fetch_data(
            '''
            SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
            clients.kra_pin,clients.date_added  
            FROM clients WHERE clients.client_status = ? ORDER BY id DESC''',(0,))
        
        self.clients_table.delete(*self.clients_table.get_children())
        for i, row in enumerate(data):
            self.clients_table.insert("", "end", values=[i+1] + list(row))

    def open_client_add_form(self):
        if self.client_add_form == None or not self.client_add_form.winfo_exists():
            self.client_add_form = ClientForm(
                self,
                update_client_table=self.set_frame_data,
                database_query=self.database_query)
        else:
            self.client_add_form.focus()
        # Lift the ClientForm window to the top of the stacking order
        self.client_add_form.focus()
           

    def clients_table_on_right_click(self,event):
        item = self.clients_table.identify_row(event.y)
        col_values = self.clients_table.item(item, "values")
        if col_values == '':
            return
        client_id_no = col_values[4]
        client_fname = col_values[1]
        client_mname = col_values[2]
        client_lname = col_values[3]
        if item:
            self.clients_table.selection_set(item)
            menu = Menu(self.clients_table, tearoff=0)
            menu.add_cascade(label=f"{client_fname} {client_mname} {client_lname}",state="disabled")
            menu.add_separator()
            menu.add_command(label="Add Plot",image=self.add_icon,compound="left",command=lambda: self.add_plot_to_client(client_id_no=client_id_no))
            if self.page == 'frame_3':
                menu.add_separator()
                menu.add_command(label="Confirm client",image=self.confirm_icon,compound="left",command=lambda: self.move_to_clients(client_id_no=client_id_no) )
            menu.add_separator()
            menu.add_command(label="Edit Client",image=self.edit_icon,compound="left",command=lambda: self.delete_client(client_id_no=client_id_no) )
            menu.add_separator()
            menu.add_command(label="Delete Client",image=self.delete_icon,compound="left",command=lambda: self.delete_client(client_id_no=client_id_no) )
            menu.add_separator()
            menu.add_command(label="Cancel",image=self.cancel_icon,compound="left",)
            menu.post(event.x_root, event.y_root)
    

    def add_plot_to_client(self,client_id_no):
        if self.update_plots_table_callback is None:
            messagebox.showinfo(title="Info", message="Please double click on client to configure",parent=self)
            return  
        if self.add_plot_to_client_form == None or not self.add_plot_to_client_form.winfo_exists():
            self.add_plot_to_client_form = PlotForm(
                self,
                database_query=self.database_query,
                client_id_no=client_id_no,
                update_plots_table_callback=self.update_plots_table_callback
            )
        else:
            self.add_plot_to_client_form.focus()
    
    def move_to_clients(self,client_id_no):
        if self.update_plots_table_callback is None:
            messagebox.showinfo(title="Info", message="Please double click on client to configure",parent=self)
            return
        
        if self.add_plot_to_client_form == None or not self.add_plot_to_client_form.winfo_exists():
            self.add_plot_to_client_form = MoveToClientsForm(
                self,
                height=120,
                database_query=self.database_query,
                client_id_no=client_id_no,
                hydrate_clients_table=self.set_frame_data,
                hydrate_plots_table=self.update_plots_table_callback
            )

        else:
            self.add_plot_to_client_form.focus()


    def get_client_report(self,client_id_no):
        print("Printing report")
    
    def delete_client(self,client):
        print("Deleting Client")
    

    def show_plots_for_client(self):
        # Clear the selection in the clients table so that no row is highlighted.
        self.clients_table.selection_remove(self.clients_table.focus())
        # Get the ID of the selected client.
        selected_client = self.clients_table.focus()
        if not selected_client:
            return
        # Create a new page to display the plots.
        if self.plots_section is not None:
            self.plots_section.destroy()

        self.client_fname = self.clients_table.item(selected_client)['values'][1]
        self.client_mname = self.clients_table.item(selected_client)['values'][2]
        self.client_lname = self.clients_table.item(selected_client)['values'][3]
        self.client_id_no = self.clients_table.item(selected_client)['values'][4]

        #create clients table
        self.plots_section = PlotFrame(
            self,
            client_fname=self.client_fname,
            client_mname=self.client_mname,
            client_lname=self.client_lname,
            client_id_no=self.client_id_no,
            database_query=self.database_query,
            
            edit_icon = self.edit_icon,
            delete_icon = self.delete_icon,
            pay_icon = self.pay_icon,
            cancel_icon=self.cancel_icon,
            transfer_icon=self.transfer_icon,

            hydrate_clients_table = self.set_frame_data

        )

        self.update_plots_table_callback = self.plots_section.hydrate_plots_table
      
        self.columnconfigure(0, weight=1)  # Set the weight of the first column of the parent frame to 1
        self.rowconfigure(1, weight=1)

        self.plots_section.grid(row=2, column=0, sticky="nsew")

    