import customtkinter
from tkinter import messagebox

class ClientForm(customtkinter.CTkToplevel):
    def __init__(self,*args,database_query,update_data_callback,**kwargs):
        super().__init__(*args, **kwargs)
        # self.geometry("520x250") #width x height
        self.resizable(False,False)
        self.attributes('-topmost', 'true')
        self.title = "Create a new client"
        self.width = 520
        self.height = 250
        # Set the geometry of the window.
        self.geometry(f"{self.width}x{self.height}")
        self.database_query = database_query
        self.update_data_callback = update_data_callback

        entry_texts = [
            "Enter First Name", "Enter Middle Name",
            "Enter Last Name","Enter ID No.",
            "Enter KRA pin","Enter Phone No."]
        
        entry_col_indices = [0, 1, 0, 1, 0, 1]  # column indices to place the entries in, in sequence
        entry_row_indices = [0, 0, 1, 1, 2, 2]  # row indices to place the entries in, in sequence
        self.form_fields = []  # create an empty list to hold the form fields

        for i in range(len(entry_texts)):
            entry = customtkinter.CTkEntry(
                master=self,
                placeholder_text=entry_texts[i],
                width=220,
                border_width=1,
                corner_radius=0
            )
            entry.grid(row=entry_row_indices[i], column=entry_col_indices[i], sticky='nsew', padx=20, pady=20)
            self.form_fields.append(entry)  # append the form field to the list
        
        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.get_form_data)

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)

    def get_form_data(self):
        form_data = {}
        
        for i, field in enumerate(self.form_fields):
            # check if all fields are filled in
            
            form_data[f"entry_{i}"] = field.get()

        if '' in form_data.values():
            messagebox.showerror(title="Error", message="All fields are required Please fill in all fields",parent=self)
            return
        
        # print(form_data)
        if self.database_query.client_exists(id_number=form_data['entry_3'],kra_pin=form_data['entry_4']):
            messagebox.showerror(title="Error", message="A client with that ID No/KRA PIN. already exists !",parent=self)
            return 

        self.database_query.execute_query(   
        
        "INSERT INTO clients (first_name,middle_name,last_name,id_number,kra_pin,phone_number) VALUES (?,?,?,?,?,?)",
        
        (form_data['entry_0'],form_data['entry_1'], form_data['entry_2'],form_data['entry_3'],form_data['entry_4'],form_data['entry_5']))
        
        data=self.database_query.fetch_data(
            '''SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
            clients.kra_pin  
            FROM clients WHERE clients.client_status = ?''',('0'))
        
        self.update_data_callback(data=data)
        self.destroy()  # close the ClientForm window
    
    



        