from fpdf import FPDF
import os
from fpdf.fonts import FontFace
from customtkinter import CTkFrame,CTkEntry,CTkButton
from app.widgets.tree_view import TreeView
from tkinter import Menu
import datetime
from PIL import Image

class PDF(FPDF):
    def __init__(self, title,total_paid,total_due,orientation="landscape", format="A4"):
        super().__init__(orientation,'mm', format)
        self.title = title
        self.total_paid= '{:,}'.format(total_paid)
        self.total_due= '{:,}'.format(total_due)

    def header(self) -> None:
        self.img = Image.open('images/transaction.png')
      
        self.set_font('helvetica','B',10)
        self.cell(0,10,f'{self.title} FULL TRANSACTIONS REPORT {datetime.datetime.now().strftime("%Y-%m-%d")}',border=False,ln=True,align='C')
        self.cell(0,8,f'TOTAL AMOUNT PAID: {self.total_paid} ksh',border=False,ln=True,align='C')
        self.cell(0,4,f'TOTAL AMOUNT DUE: {self.total_due} ksh',border=False,ln=True,align='C')
        self.image(self.img, 10, 8, 25)
        self.img = self.img.rotate(180, expand=True)
        self.img = self.img.transpose(Image.FLIP_TOP_BOTTOM)
        self.image(self.img, 262, 8, 25)
        self.ln(5)
    
    def footer(self):
        self.set_y(-15)
        self.set_font('helvetica','I',10)
        self.cell(0,10, f"Page {self.page_no()}",align='C')

class ReportFrame(CTkFrame):
    def __init__(self,*args,database_query,**kwargs):
        super().__init__(*args, **kwargs)
        self.corner_radius = 0 
        self.fg_color = "transparent"
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.database_query = database_query
        self.reports_folder = os.path.expanduser("~/Documents/kaingaprix/Reports/")
        if not os.path.exists(self.reports_folder):
            os.makedirs(self.reports_folder)
        self.user_add_form = None
         # Create search bar
        self.search_bar = CTkEntry(
            self,
            width=250, fg_color="white",text_color="black",
            placeholder_text="Search by ID number or KRA pin")
        
        self.search_bar.grid(row=0, column=0, pady=(22,0),padx=(0,20), sticky="e")


        self.full_report_button = CTkButton(master=self, text="Printer",command=self.print_full_report)
            
        self.clients_reports_table = TreeView(self, columns=['#'] + ["First name","Middle Name", "Last name", "ID No.","Folio No","Member No.","Date added","Member since"])
        self.clients_reports_table.column("#", width=20, minwidth=20)
        self.clients_reports_table.grid(row=1,column=0,sticky="nsew",pady=(10,20),padx=(0,20)) 
        self.clients_reports_table.bind("<Button-3>", self.reports_table_on_right_click)

        self.hydrate_clients_reports_table()
    
    def hydrate_clients_reports_table(self):
        
        self.data =  self.database_query.fetch_data(
        '''
        SELECT clients.first_name,clients.middle_name,clients.last_name,clients.id_number,
        clients.folio_number,clients.member_number,clients.date_added,clients.member_since 
        FROM clients ORDER BY id DESC

        ''')

        self.clients_reports_table.delete(*self.clients_reports_table.get_children())
        for i, row in enumerate(self.data):
            self.clients_reports_table.insert("", "end", values=[i+1] + list(row))
   
    def print_full_report(self):
        print("report table right click")
    
    def reports_table_on_right_click(self,event):
        item = self.clients_reports_table.identify_row(event.y)
        col_values = self.clients_reports_table.item(item, "values")
        if col_values == '':
            return
        client_id_no = col_values[4]
        client_fname = col_values[1]
        client_mname = col_values[2]
        client_lname = col_values[3]
        
        if item:
            self.clients_reports_table.selection_set(item)
            menu = Menu(self.clients_reports_table, tearoff=0)
            menu.add_cascade(label=f"{client_fname} {client_mname} {client_lname}",state="disabled")
            menu.add_separator()
            menu.add_command(label="Transactions report",command=lambda: self.transaction_report(client_id_no=client_id_no))
            menu.add_separator()
            menu.add_command(label="Property report",command=lambda: self.property_report(client_id_no=client_id_no))
            menu.add_separator()
            menu.add_command(label="Cancel",compound="left",)
            menu.post(event.x_root, event.y_root)

    def transaction_report(self,client_id_no):
        blue = (0, 0, 255)
        grey = (255, 255, 255)
        self.greyscale =200
        self.headings_style = FontFace(color=blue, fill_color=grey)

        transactions_summary = self.database_query.fetch_data(
        '''
           SELECT SUM(total_amount_paid),SUM(total_amount_due) FROM policies WHERE client_id_no = ?;
        ''',(client_id_no,))

        transaction_summary = transactions_summary[0]
        self.pdf = PDF(title=client_id_no,total_paid=transaction_summary[0],total_due=transaction_summary[1])
        self.pdf.page_mode = "FULL_SCREEN"
        self.pdf.set_font("Times", size=12)
        self.pdf.add_page()
       
        transactions = self.database_query.fetch_data(
            '''
            SELECT 
            printf('%,d', PL.plot_price) || '.00 ksh' AS plot_price_formatted,
            printf('%,d', T.transaction_amount) || '.00 ksh' AS amount_formatted,
            printf('%,d', T.transaction_total_amount_paid) || '.00 ksh' AS total_amount_paid_formatted,
            printf('%,d', T.transaction_total_amount_due) || '.00 ksh' AS total_amount_due_formatted,
            T.transaction_mode, 
            P.plot_ref_no, 
            T.transaction_date
            FROM transactions T
            JOIN policies P ON T.policy_id = P.id
            JOIN plots PL ON P.plot_ref_no = PL.plot_ref_no
            WHERE P.client_id_no = ?
            ORDER BY T.id ASC
            ''',(client_id_no,))
        
      
        # Generate a sequence of (index, transaction) pairs, and convert each transaction to a tuple of strings
        table_data = (
                ("#","Total balance","Transacted amount","Total paid","New balance","Transaction mode","Policy","Transaction date"),
                *(tuple(map(str, (i, *t))) for i, t in enumerate(transactions, 1))
            )
            
        with self.pdf.table(cell_fill_mode="ROWS",line_height=1.6 * self.pdf.font_size,col_widths=(10,18,24,20,20,20,20,21)) as table:
            for data_row in table_data:
                row = table.row()                
                for datum in data_row:
                    row.cell(datum)
        
        date_now = datetime.datetime.now().strftime("%Y-%m-%d")
        self.pdf.output(f'{self.reports_folder}/TRANS-{client_id_no}-{date_now}.pdf')

    def property_report(self,client_id_no):
        print("Property report")
   







       
