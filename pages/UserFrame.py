from customtkinter import CTkFrame,CTkEntry,CTkButton
from app.widgets.tree_view import TreeView
from pages.Forms import AddNewUser

class UserFrame(CTkFrame):
    def __init__(self,*args,database_query,**kwargs):
        super().__init__(*args, **kwargs)
        self.corner_radius = 0 
        self.fg_color = "transparent"
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.database_query = database_query
        self.user_add_form = None

        self.add_user_button = CTkButton(master=self, text="New user",command=self.open_user_add_form)
            
        self.add_user_button.grid(row=0, column=0, pady=(25,0), padx=10, sticky="w")

        self.users_table = TreeView(self, columns=['#'] + ["First Name","Last Name","E-mail","Rank","Date Added"])
        self.users_table.column("#", width=20, minwidth=20)
        self.users_table.grid(row=1,column=0,sticky="nsew",pady=(10,20),padx=(0,20)) 
        self.users_table.bind("<Button-3>", self.users_table_on_right_click )

        self.hydrate_user_table()
    
    def hydrate_user_table(self):     
        self.data =  self.database_query.fetch_data(
        '''
        SELECT login.login_first_name,
        login.login_last_name,
        login.login_email,
        CASE login.login_rank 
            WHEN 0 THEN 'Administrator' 
            WHEN 1 THEN 'Clerk' 
            ELSE 'Super administrator' 
        END AS login_rank_text,
        login.login_date_added
            FROM login
            WHERE login.login_email != 'default'
            ORDER BY id DESC
        '''
    )

        self.users_table.delete(*self.users_table.get_children())
        for i, row in enumerate(self.data):
            self.users_table.insert("", "end", values=[i+1] + list(row))
   

    def users_table_on_right_click(self,event):
        item = self.users_table.identify_row(event.y)
        col_values = self.users_table.item(item, "values")
        if col_values == '':
            return
        print("Users table right click")

    def open_user_add_form(self):
        if self.user_add_form == None or not self.user_add_form.winfo_exists():
            self.user_add_form = AddNewUser(
                self,
                hydrate_users_table=self.hydrate_user_table,
                database_query=self.database_query
            )
        else:
            self.user_add_form.focus()
        # Lift the ClientForm window to the top of the stacking order
        self.user_add_form.focus()

       
