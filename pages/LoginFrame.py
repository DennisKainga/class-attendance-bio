from customtkinter import CTkFrame, CTkEntry, CTkButton, CTkFont, CTkLabel

class LoginFrame(CTkFrame):
    def __init__(self, *args, image,login_callback=None,**kwargs):
        super().__init__(*args, **kwargs)
        self.corner_radius = 0 
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.image = image
        self.show_case_image = CTkLabel(self, text="", image=self.image)
        self.show_case_image.grid(row=0,column=0, padx=20, pady=10)

        self.email_entry = CTkEntry(
            master=self, 
            width=400, height=40, placeholder_text="Enter E-mail")
        
        self.email_entry.grid(row=1,column=0,pady=20)
    
        self.password_entry = CTkEntry(master=self,
            width=400, height=40, placeholder_text="Enter Password", show="*")
        
        self.password_entry.grid(row=2,column=0,pady=20)

        self.login_button = CTkButton(master=self,
            width=400, height=50, font=CTkFont(size=20), text="LOGIN", command=self.login)
        
        self.login_callback = login_callback
        
        self.login_button.grid(row=3,column=0,pady=20)
        
    def login(self):
        if self.login_callback:
            password = self.password_entry.get()
            email = self.email_entry.get()
            
            self.login_callback(email,password )
