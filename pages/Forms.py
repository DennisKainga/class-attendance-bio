import customtkinter
import datetime
from tkinter import messagebox
import bcrypt

"""This is the parent form class that contains the basic validations for all form inputs"""
class Forms(customtkinter.CTkToplevel):
    def __init__(self,*args,width=None,height=None,**kwargs):
        super().__init__(*args, **kwargs)
        # self.geometry("520x250") #width x height
        self.resizable(False,False)
        self.attributes('-topmost', 'true')
        self.title = "Create a new client"
        self.width = width or 520
        self.height = height or 250
        # Set the geometry of the window.
        self.geometry(f"{self.width}x{self.height}")
        self.form_fields = []

    def generate_form_fields(self,entry_texts,entry_row_indices,entry_col_indices):
        for i in range(len(entry_texts)):
            entry = customtkinter.CTkEntry(
                master=self,
                placeholder_text=entry_texts[i],
                width=220,
                border_width=1,
                corner_radius=0
            )
            entry.grid(row=entry_row_indices[i], column=entry_col_indices[i], sticky='nsew', padx=20, pady=20)
            self.form_fields.append(entry)  # append the form field to the list
    
    def update_client_status(self,new_status=None):
         # get ID of updated policy using SELECT statement
        self.database_query.execute_query(
            '''
            SELECT client_status 
            FROM clients 
            WHERE id_number = ?
            ''',
            (self.client_id_no,)
        )

        #fetch client status
        client_status = self.database_query.cursor.fetchone()[0]

        if client_status !=2:
            #update client status
            self.database_query.execute_query(   
                '''
                UPDATE clients
                SET client_status = ?  
                WHERE id_number = ? ''',
                (new_status,self.client_id_no)
            )

        #rehydrate the client plot table
        if client_status == 0:
            self.hydrate_clients_table()
                        
            
        if client_status == 1 or client_status == 2:
            self.hydrate_clients_table()
            self.hydrate_plots_table() 

        self.destroy()  # close the PlotsForm window
        
    
    def get_form_data(self):
        form_data = {}
        for i, field in enumerate(self.form_fields):
            # check if all fields are filled in
            form_data[f"entry_{i}"] = field.get()
        if '' in form_data.values():
            messagebox.showerror(title="Error", message="All fields are required Please fill in all fields",parent=self)
            return
        return form_data




"""This generate the client form and does all the basic validation it also re hydrates the clients table"""
class ClientForm(Forms):
    def __init__(self,*args,update_client_table,database_query,**kwargs):
        super().__init__(*args, **kwargs)
        self.database_query = database_query
        self.update_client_table = update_client_table

        entry_texts = [
            "Enter First Name", "Enter Middle Name",
            "Enter Last Name","Enter ID No.","Enter KRA pin","Enter Phone No."
        ]
        entry_col_indices = [0, 1, 0, 1, 0, 1]  # column indices to place the entries in, in sequence
        entry_row_indices = [0, 0, 1, 1, 2, 2]  # row indices to place the entries in, in sequence

        self.generate_form_fields(
            entry_texts=entry_texts,
            entry_col_indices=entry_col_indices,
            entry_row_indices=entry_row_indices
        )

        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.submit_form_data)

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)

    def submit_form_data(self):
        form_data = self.get_form_data()
        if form_data is None:
            return
        if not (form_data['entry_3'].isnumeric()):
            messagebox.showerror(title="Error", message="A client with that ID must be a number !",parent=self)
            return 

        #Check if client exists in database
        if self.database_query.client_exists(id_number=form_data['entry_3'],kra_pin=form_data['entry_4']):
            messagebox.showerror(title="Error", message="A client with that ID No/KRA PIN. already exists !",parent=self)
            return 
        
        self.database_query.execute_query(   
        "INSERT INTO clients (first_name,middle_name,last_name,id_number,kra_pin,phone_number) VALUES (?,?,?,?,?,?)",
        (form_data['entry_0'],form_data['entry_1'], 
         form_data['entry_2'],form_data['entry_3'],form_data['entry_4'],form_data['entry_5']))
        
        #fetch new inserted data        
        self.update_client_table()
        self.destroy()  # close the ClientForm window
    


"""This generate the client's form the method from the main form class does the validation"""
class PlotForm(Forms):
    def __init__(self,*args,database_query,client_id_no,update_plots_table_callback,**kwargs):
        super().__init__(*args,**kwargs)
        entry_texts = ["Enter plot name", "Enter plot ID","Enter address","Enter plot size","Enter plot price"]
        entry_col_indices = [0, 1, 0, 1, 0]  # column indices to place the entries in, in sequence
        entry_row_indices = [0, 0, 1, 1, 2]  # row indices to place the entries in, in sequence
        self.client_id_no = client_id_no
        self.update_plots_table_callback=update_plots_table_callback
        
        self.database_query = database_query
        self.form_fields = []  # create an empty list to hold the form fields
        
        self.generate_form_fields(
            entry_texts=entry_texts,
            entry_col_indices=entry_col_indices,
            entry_row_indices=entry_row_indices
        )
        
        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.submit_form_data
        )

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)
    
    def submit_form_data(self):
        form_data = self.get_form_data()
        if form_data is None:
            return
        
        if not (form_data['entry_4'].isnumeric()):
            messagebox.showerror(title="Error", message="The plot price field must be a number !",parent=self)
            return
        
        plot_price = int(form_data['entry_4'])
        plot_ref_no = str(form_data['entry_1'])
        #check if plot exists usig the ref no
         #Check if client exists in database
        if self.database_query.plot_exists(plot_ref_no=plot_ref_no):
            messagebox.showerror(title="Error", message="Plot already assigned to client try transfering!",parent=self)
            return 

        #Add plot
        self.database_query.execute_query(   
            '''INSERT INTO plots 
            (plot_name,plot_ref_no,plot_address,plot_size,plot_price,client_id_no) 
            VALUES (?,?,?,?,?,?)''',
            (
            form_data['entry_0'],plot_ref_no,form_data['entry_2'],
            form_data['entry_3'],plot_price,self.client_id_no
        ))

        #create policy for plot
        self.database_query.execute_query(   
            
            '''INSERT INTO policies (total_amount_due,total_amount_paid,plot_ref_no,client_id_no) 
            VALUES (?,?,?,?)''',(
            plot_price,
            0,
            form_data['entry_1'],
            self.client_id_no
        ))
        
        #rehydrate the client plot table
        self.update_plots_table_callback()
                
        self.destroy()  # close the PlotsForm window


"""This generates make payment form"""
class PaymentForm(Forms):
    def __init__(self,*args,database_query,plot_ref_no,client_id_no,hydrate_clients_table,hydrate_plots_table,**kwargs):
        super().__init__(*args,**kwargs)
        self.database_query = database_query
        
        self.plot_ref_no = plot_ref_no
        self.client_id_no = client_id_no
        self.hydrate_clients_table = hydrate_clients_table
        self.hydrate_plots_table = hydrate_plots_table
        

        entry_texts = ["Enter payment amount", "payment Mode"]
        entry_col_indices = [0, 1]  # column indices to place the entries in, in sequence
        entry_row_indices = [0, 0]  # row indices to place the entries in, in sequence

        self.generate_form_fields(
            entry_texts=entry_texts,
            entry_col_indices=entry_col_indices,
            entry_row_indices=entry_row_indices
        )

        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.submit_form_data
        )

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)

    # get policy using SELECT statement
    def get_policy(self):
        self.database_query.execute_query(
            '''
            SELECT id,total_amount_paid,total_amount_due
            FROM policies 
            WHERE plot_ref_no LIKE ? AND client_id_no = ?
            ''',
            (self.plot_ref_no, self.client_id_no)
        )

        return self.database_query.cursor.fetchone()
    
    #Get the plot in question
    def get_plot(self):
        
        self.database_query.execute_query(
            '''
            SELECT id,plot_price
            FROM plots
            WHERE plot_ref_no LIKE ?
            ''',
            (self.plot_ref_no,)
        )

        return self.database_query.cursor.fetchone()
    

    def submit_form_data(self):
        form_data = self.get_form_data()

        if form_data is None:
            return
        
        if not (form_data['entry_0'].isnumeric()):
            messagebox.showerror(title="Error", message="The amount must be a number !",parent=self)
            return
        
        if not (int(form_data['entry_0']) > 0):
            messagebox.showerror(title="Error", message="The amount must be a positive number !",parent=self)
            return
        
        form_amount = int(form_data['entry_0'])
        transaction_mode = form_data['entry_1']
        
        """This gets the plolicy information"""
        policy_result =  self.get_policy()
        plot_maximum_price = policy_result[2]
        
        if form_amount > plot_maximum_price:
            messagebox.showerror(title="Error", message=f"The maximum allowed price is {plot_maximum_price} ksh !",parent=self)
            return

        if policy_result[2] == 0:
            messagebox.showerror(title="Error", message="This plot can not receive anymore payments !",parent=self)
            return

        #update policy amount
        self.database_query.execute_query(   
            '''
            UPDATE policies 
            SET total_amount_due = total_amount_due - ? , total_amount_paid = total_amount_paid + ? 
            WHERE plot_ref_no LIKE ? AND client_id_no = ? ''',
            (form_amount,form_amount,self.plot_ref_no,self.client_id_no)
        )


        policy_result =self.database_query.fetch_data(
            '''
            SELECT id,total_amount_paid,total_amount_due
            FROM policies 
            WHERE plot_ref_no LIKE ? AND client_id_no = ?
            '''
        ,(self.plot_ref_no, self.client_id_no))

        policy_id = policy_result[0][0]
        total_amount_paid = policy_result[0][1]
        total_amount_due = policy_result[0][2]

        self.database_query.execute_query(   
            '''
            INSERT INTO transactions (
            transaction_amount,
            transaction_total_amount_paid,
            transaction_total_amount_due,
            transaction_mode,
            policy_id
            )
            VALUES (?,?,?,?,?)
            ''',
            (form_amount,total_amount_paid,total_amount_due,transaction_mode,policy_id)
        )

        self.update_client_status(new_status=1)
   
      
        
"""This generates make payment form"""
class MoveToClientsForm(Forms):
    def __init__(self,*args,database_query,client_id_no,hydrate_clients_table,hydrate_plots_table,**kwargs):
        super().__init__(*args,**kwargs)
        self.database_query = database_query
    
        self.client_id_no = client_id_no
        self.hydrate_clients_table = hydrate_clients_table
        self.hydrate_plots_table = hydrate_plots_table
       
        entry_texts = ["Enter member number", "Enter folio number"]
        entry_col_indices = [0, 1]  # column indices to place the entries in, in sequence
        entry_row_indices = [0, 0]  # row indices to place the entries in, in sequence

        self.generate_form_fields(
            entry_texts=entry_texts,
            entry_col_indices=entry_col_indices,
            entry_row_indices=entry_row_indices
        )

        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.submit_form_data
        )

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)
    
    def submit_form_data(self):
        form_data = self.get_form_data()

        if form_data is None:
            return
        
        member_number = form_data['entry_0']
        folio_number = form_data['entry_1']
        member_since = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")


        #Update client and give member number and folio number
        self.database_query.execute_query(   
            '''
            UPDATE clients 
            SET member_number = ? , folio_number = ?, member_since = ?
            WHERE id_number = ? ''',

            (member_number,folio_number,member_since,self.client_id_no)
        )

        self.update_client_status(new_status=2)
        self.hydrate_clients_table()



"""This generates make payment form"""
class AddNewUser(Forms):
    def __init__(self,*args,database_query,hydrate_users_table,**kwargs):
        super().__init__(*args,**kwargs)
        self.database_query = database_query
    
        self.hydrate_users_table = hydrate_users_table    

        entry_texts = [
            "Enter first name", 
            "Enter last name",
            "Enter Email",
            "Enter password",
            "Repeat Password"
        ]
        
        entry_row_indices = [0, 0, 1, 1, 2]  # row indices to place the entries in, in sequence
        
        entry_col_indices = [0, 1, 0, 1, 0]  # column indices to place the entries in, in sequence

        self.generate_form_fields(
            entry_texts=entry_texts,
            entry_col_indices=entry_col_indices,
            entry_row_indices=entry_row_indices
        )

        # create a dictionary to map the values to their corresponding indices
        self.ranks_map = {"Administrator": 0,"Clerk": 1}
      

        self.ranks_entry = customtkinter.CTkOptionMenu(self,values=["Administrator", "Clerk"],
                                                  command=self.optionmenu_callback)
        self.ranks_entry.grid(row=2,column=1,sticky='nsew', padx=20, pady=20)
        # set the default value to "Administrator"
        self.ranks_entry.set("Clerk")

    
        #create a submit button
        self.submit_button = customtkinter.CTkButton(master=self,
            width=120,
            height=32,
            border_width=0,
            corner_radius=8,
            text="Submit",
            command=self.submit_form_data
        )

        self.submit_button.grid(row=3,column=0,sticky='nsew',padx= 20)
    
    def optionmenu_callback(self,rank):
        print("optionmenu dropdown clicked:", rank)
    
    def submit_form_data(self):
        form_data = self.get_form_data()
        if form_data is None:
            return
        
         # get the selected value and its index
        selected_value = self.ranks_entry.get()
        login_rank = int(self.ranks_map[selected_value])
        first_name = form_data['entry_0']
        last_name = form_data['entry_1']
        email = form_data['entry_2']
        password = str(form_data['entry_3'])
        password_repeat = form_data['entry_4']

        if self.database_query.user_exists(login_email=email) != False:
            messagebox.showerror(title="Error", message="A user with that email already exists!",parent=self)
            return

        if password != password_repeat:
            messagebox.showerror(title="Error", message="Passwords do not match!",parent=self)
            return

       
        # generate a salt   
        
        salt = bcrypt.gensalt()
        login_password = bcrypt.hashpw(password.encode(), salt)
        
        self.database_query.execute_query(   
            '''
            INSERT INTO login 
            (login_first_name,login_last_name,login_email,login_rank,login_password)
            VALUES (?,?,?,?,?)
            ''',
            (first_name,last_name,email,login_rank,login_password)
        )

        self.hydrate_users_table()

        self.destroy()

    

