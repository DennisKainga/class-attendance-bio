from customtkinter import CTkFrame,CTkLabel,CTkFont
from app.widgets.tree_view import TreeView
from pages.Forms import PaymentForm
from tkinter import END,Menu

class PlotFrame(CTkFrame):
    def __init__(self,*args,client_fname,
                 client_mname,client_lname,
                 client_id_no,database_query,
                 hydrate_clients_table,
                 edit_icon,
                 delete_icon,
                 pay_icon,
                 cancel_icon,
                 transfer_icon,
                 **kwargs):
        super().__init__(*args,**kwargs)
        self.height = 800
        self.corner_radius = 0 
        self.fg_color = "transparent"
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        
        self.client_fname = client_fname
        self.client_mname = client_mname
        self.client_lname = client_lname
        self.client_id_no = client_id_no
        self.database_query = database_query
        self.record_plot_payment_form = None
        self.hydrate_clients_table = hydrate_clients_table

        self.edit_icon = edit_icon
        self.delete_icon = delete_icon
        self.pay_icon = pay_icon
        self.cancel_icon =cancel_icon
        self.transfer_icon =transfer_icon
        
        self.page_title = CTkLabel(
            self, text=f"{self.client_fname} {self.client_mname} {self.client_lname}", 
            font=CTkFont(size=20, weight="bold"))
        
        self.page_title.grid(row=0, column=0, sticky="nsew",pady=20)

        self.plots_table = TreeView(self, columns=['#'] + [
            "Plot name","Plot Id","Address","Size","Initial Price","Total amount due","Total amount paid","Date Inquired"])

        self.plots_table.column("#", width=20, minwidth=20)

        self.plots_table.grid(row=1,column=0,sticky="nsew",pady=(0,25),padx=(0,20)) 
        self.plots_table.bind("<Button-3>", self.plots_table_on_right_click)

        self.hydrate_plots_table()
    
    def hydrate_plots_table(self,push=False):
        client_id_no = int(self.client_id_no)
        if push:
            data = []
            print('Pushed')
            return
        
        if not push:
            data=self.database_query.fetch_data(
                '''
                SELECT plots.plot_name, plots.plot_ref_no, plots.plot_address, plots.plot_size, plots.plot_price,
                policies.total_amount_due, policies.total_amount_paid ,policies.date_created
                FROM plots 
                INNER JOIN policies ON policies.plot_ref_no = plots.plot_ref_no AND policies.client_id_no = ?
                WHERE plots.client_id_no = ? 
                ORDER BY plots.id DESC''',(client_id_no,client_id_no)
            ) 

        self.plots_table.delete(*self.plots_table.get_children())
        for i, row in enumerate(data):
            self.plots_table.insert("", "end", values=[i+1] + list(row))
    
    def plots_table_on_right_click(self,event):
        item = self.plots_table.identify_row(event.y)
        col_values = self.plots_table.item(item, "values")
        # get the plot ID from the second column (index 1)
        plot_ref_no = col_values[2]
        plot = 0
        if item:
            self.plots_table.selection_set(item)
            menu = Menu(self.plots_table, tearoff=0)
            menu.add_command(label="Record payment",image=self.pay_icon,compound='left',command=lambda: self.record_payment(plot_ref_no=plot_ref_no) )
            menu.add_separator()
            menu.add_command(label="Transfer plot ownership",image=self.transfer_icon,compound='left',command=lambda: self.transfer_plot(plot_ref_no=plot_ref_no) )
            menu.add_separator()
            menu.add_command(label="Edit plot",image=self.edit_icon,compound='left',command=lambda: self.delete_plot(plot_ref_no=plot_ref_no) )
            menu.add_separator()
            menu.add_command(label="Delete plot",image=self.delete_icon,compound='left',command=lambda: self.delete_plot(plot_ref_no=plot_ref_no) )
            menu.add_separator()
            menu.add_command(label="Cancel",image=self.cancel_icon,compound='left')
            menu.post(event.x_root, event.y_root)
    

    def copy_plot(self,plot_ref_no):
        print("Copying plot")
    
    def record_payment(self,plot_ref_no):
        if self.record_plot_payment_form == None or not self.record_plot_payment_form.winfo_exists():
            self.record_plot_payment_form = PaymentForm(
                self,
                height=120,
                database_query= self.database_query,
                client_id_no = self.client_id_no,
                plot_ref_no = plot_ref_no,
                hydrate_clients_table = self.hydrate_clients_table,
                hydrate_plots_table = self.hydrate_plots_table,
            )

        else:
            self.record_plot_payment_form.focus()
    
    def transfer_plot(self,plot_ref_no):
        print('transfering plot')
    
    def delete_plot(self,plot_ref_no):
        print("Deleting")
