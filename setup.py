import sys
from cx_Freeze import Executable,setup

base = None

if sys.platform == "win32":
    base = "Win32GUI"

# http://msdn.microsoft.com/en-us/library/windows/desktop/aa371847(v=vs.85).aspx
shortcut_table = [(
    "DesktopShortcut",        # Shortcut
    "DesktopFolder",          # Directory_
    "Land Management",           # Name that will be show on the link
    "TARGETDIR",              # Component_
    "[TARGETDIR]land management.exe",# Target exe to exexute
    None,                     # Arguments
    None,                     # Description
    None,                     # Hotkey
    None,                     # Icon
    None,                     # IconIndex
    None,                     # ShowCmd
    'TARGETDIR'               # WkDir
    )]

# Now create the table dictionary
msi_data = {"Shortcut": shortcut_table}

# Change some default MSI options and specify the use of the above defined tables
bdist_msi_options = {
    'data': msi_data,
    'initial_target_dir': 'C:\\Kaingaprix\\Lands'
    }

build_exe_options = {
    'optimize': 2,  # 0=none, 1=bytecode, 2=docstrings
    'packages': ['tkinter','customtkinter','PIL','bcrypt','fpdf'],
    'includes': [],
    'include_files': ['images/'],
    'zip_includes': [],
    'zip_include_packages': [],
    'excludes': ['numpy', 'matplotlib']
}

setup(
    name = "Land management v1",
    version="0.1",
    description = "Land management\ncopyright 2023 KaingaPrix",
    
    options={
        "bdist_msi": bdist_msi_options,
        "build_exe": build_exe_options
    },
    executables=[Executable("main.py", base=base,targetName="Land management",icon ="images/main.ico")],
)
