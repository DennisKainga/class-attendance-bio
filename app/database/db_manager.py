import sqlite3
import os
import bcrypt


class DatabaseManager:
    def __init__(self) -> None:
        # self.connection = sqlite3.connect('app/database/database.sqlite')
        documents_folder = os.path.expanduser("~/Documents/")
        settings_folder = os.path.join(documents_folder, "kaingaprix/settings")
        
        if not os.path.exists(settings_folder):
            os.makedirs(settings_folder)

        db_filename = "database.db"
        db_path = os.path.join(settings_folder, db_filename)
        self.connection = sqlite3.connect(db_path)
        self.cursor = self.connection.cursor()
        self.create_tables()
    
        # check if the login table is empty, and create a default admin user if it is
        if not self.cursor.execute("SELECT 1 FROM login LIMIT 1").fetchone():
            default_admin = {
                'login_first_name': 'Default',
                'login_last_name': 'Admin',
                'login_email': 'admin',
                'login_rank': 0,  # 0 for  administrator
                'login_password': bcrypt.hashpw('admin'.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')
            }       
            self.execute_query(
                '''INSERT INTO login(login_first_name, login_last_name, login_email, login_rank, login_password)
                VALUES(?, ?, ?, ?, ?)''', 
                (
                 default_admin['login_first_name'],
                 default_admin['login_last_name'],
                 default_admin['login_email'],
                 default_admin['login_rank'],
                 default_admin['login_password']
                ))
            
    def create_tables(self):
        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS login (
            id INTEGER PRIMARY KEY,
            login_first_name TEXT,
            login_last_name TEXT,
            login_email TEXT,
            login_rank INTEGER,
            login_password BYTES,
            login_date_added TEXT DEFAULT (strftime('%Y-%m-%d %H:%M:%S', 'now','localtime'))
            );
        ''')

        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS session (
            id INTEGER PRIMARY KEY,
            login_rank INTEGER,
            login_id INTEGER
            );
        ''')
        
        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS clients (
            id INTEGER PRIMARY KEY,
            first_name TEXT,
            middle_name TEXT,
            last_name TEXT,
            id_number INTEGER,
            kra_pin TEXT,
            phone_number TEXT,
            date_added TEXT DEFAULT (strftime('%Y-%m-%d %H:%M:%S', 'now','localtime')),
            member_number TEXT DEFAULT 'None',
            member_since TEXT DEFAULT '0-0-0',
            folio_number TEXT DEFAULT 'None',
            client_status INTERGER DEFAULT 0
            );
            ''')
        
        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS plots (
            id INTEGER PRIMARY KEY,
            plot_ref_no TEXT,
            plot_name TEXT,
            plot_address TEXT,
            plot_size TEXT,
            plot_price INTEGER,
            client_id_no INTEGER,
            FOREIGN KEY(client_id_no) REFERENCES clients(id_number)
            );
            '''
        )

        self.cursor.execute(
            '''
            CREATE TABLE IF NOT EXISTS policies (
            id INTEGER PRIMARY KEY,
            total_amount_due INTEGER,
            total_amount_paid INTEGER,
            plot_ref_no TEXT,
            client_id_no INTEGER,
            date_created TEXT DEFAULT (strftime('%Y-%m-%d %H:%M:%S', 'now','localtime')),
            FOREIGN KEY (plot_ref_no) REFERENCES plots(plot_ref_no)
            FOREIGN KEY (client_id_no) REFERENCES clients(client_id_no)
            );
            ''')

        self.cursor.execute(
            '''
           CREATE TABLE IF NOT EXISTS transactions (
            id INTEGER PRIMARY KEY,
            transaction_amount INTEGER,
            transaction_total_amount_paid INTEGER,
            transaction_total_amount_due INTEGER,
            transaction_mode TEXT,
            transaction_date TEXT DEFAULT (strftime('%Y-%m-%d %H:%M:%S', 'now')),
            policy_id INTEGER,
            FOREIGN KEY (policy_id) REFERENCES policies (id));
            ''')
    
    def execute_query(self, query, values=None):
        if values:
            self.cursor.execute(query, values)
        else:
            self.cursor.execute(query)
        self.connection.commit()

    def fetch_data(self, query, values=None):
        if values:
            self.cursor.execute(query, values)
        else:
            self.cursor.execute(query)
        return self.cursor.fetchall()

    def client_exists(self, id_number,kra_pin):
        query = '''SELECT id_number FROM clients WHERE id_number = ? OR kra_pin = ? '''
        result = self.cursor.execute(query, (id_number,kra_pin)).fetchone()
        if result is None:
            return False
        else:
            return True
    
    def plot_exists(self,plot_ref_no):
        query = '''SELECT plot_ref_no FROM plots WHERE plot_ref_no LIKE  ? '''
        result = self.cursor.execute(query,(plot_ref_no,)).fetchone()
        if result is None:
            return False
        else:
            return True
    
    def user_exists(self,login_email):
        query = '''SELECT * FROM login WHERE login_email = ? '''
        result = self.cursor.execute(query, (login_email,)).fetchone()
        if result is None:
            return False
        else:
            return result

    

    def __del__(self):
        self.connection.close()