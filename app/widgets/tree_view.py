from tkinter import ttk, CENTER, NO, W

class TreeView(ttk.Treeview):
    def __init__(self,parent,columns):
        super().__init__(parent, selectmode="extended")

        self['columns'] = columns
        for i, col in enumerate(columns, start=1):
            self.column(col, anchor=CENTER, width=80)
            self.heading(col, text=col, anchor=CENTER)
        # Hide the '#' column
        self.column("#0", width=0, stretch=NO)
        
        self.grid(row=0, column=0, sticky="nswe")
   
    def hydrate_table(self,data,table):
        table.delete(*table.get_children())
        for i, row in enumerate(data):
            table.insert("", "end", values=[i+1] + list(row))
